### 2023-12-31
- Updated SF fonts (iOS 17)
- Removed LR font
- Updated config file

[Full Changelog](https://gitlab.com/nongthaihoang/oh_my_font/-/commits/master)
